function ClickHandler(event, ships) {
    let element = event.target;
    let row = element.getAttribute('row');
    let col = element.getAttribute('col');
    let cell = row + col;

    let shipDown = [];

    for (const cellKey in ships) {
        var shipPositions = ships[cellKey].shipPosition;
        var shipName = ships[cellKey].shipName;

        console.log(shipPositions);
        console.log(cell);
        console.log(shipPositions.indexOf(cell));

        if (shipPositions.indexOf(cell) > -1) {
            var index = shipPositions.indexOf(cell);

            shipPositions.splice(index, 1);

            element.style.backgroundColor = 'blue';

            if (shipPositions.length === 0) {

                shipDown.push(ships[cellKey]);

                drawShipDown(shipDown);

                delete ships[cellKey];
            }

            break;
        } else {
            element.style.backgroundColor = 'red';
        }
    }
}

function clearBox(elementID) {
    if (elementID.innerHTML !== "") {
        document.getElementById(elementID).innerHTML = "";
    }
}


function drawShipDown(ships) {
    let wrapper = document.createElement("div");
    let txt = document.createElement("div");

    document.body.appendChild(wrapper);

    clearBox(txt);

    for (const txtKey in ships) {
        let ship = ships[txtKey];


        txt.innerHTML = ship.shipName;
    }

    wrapper.appendChild(txt);
}

function drawBoard() {
    const table_game = {
        'table': undefined,
        'data': undefined
    }

    const ref = [
        [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0],
        [0, 3, 3, 3, 3, 0, 0, 0, 0, 0, 4, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 5, 5, 5, 5, 5, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ]

    const refShip = {
            1: {
                'shipName': 'Bateau 1',
                'shipColor': 'red',
                'shipPosition': [],
                'shipCode': 1
            },
            2: {
                'shipName': 'Bateau 2',
                'shipColor': 'red',
                'shipPosition': [],
                'shipCode': 2
            },
            3: {
                'shipName': 'Bateau 3',
                'shipColor': 'red',
                'shipPosition': [],
                'shipCode': 3
            },
            4: {
                'shipName': 'Bateau 4',
                'shipColor': 'red',
                'shipPosition': [],
                'shipCode': 4
            },
            5: {
                'shipName': 'Bateau 5',
                'shipColor': 'red',
                'shipPosition': [],
                'shipCode': 5
            }
        }
    ;

    let table = document.createElement("table");
    table.style.border = '1px solid black';
    table.style.width = '800px';
    table.style.height = '800px';

    for (let i = 0; i < ref.length; i++) {
        let row = document.createElement("tr");
        let letter = String.fromCharCode(i + 65);
        for (let j = 1; j <= ref[i].length; j++) {
            let cell = document.createElement("td");
            let cellCode = letter + j;

            cell.style.border = '1px solid black';
            cell.setAttribute('class', 'cell');
            cell.setAttribute('row', letter);
            cell.setAttribute('col', j);
            row.appendChild(cell);

            switch (ref[i][j]) {
                case 1:
                    refShip[1].shipPosition.push(cellCode);
                    break;
                case 2:
                    refShip[2].shipPosition.push(cellCode);
                    break;
                case 3:
                    refShip[3].shipPosition.push(cellCode);
                    break;
                case 4:
                    refShip[4].shipPosition.push(cellCode);
                    break;
                case 5:
                    refShip[5].shipPosition.push(cellCode);
                    break;
            }

            cell.addEventListener('click', function (event) {
                ClickHandler(event, refShip);
            });
        }
        table.appendChild(row);
    }
    document.body.appendChild(table);
}

drawBoard();